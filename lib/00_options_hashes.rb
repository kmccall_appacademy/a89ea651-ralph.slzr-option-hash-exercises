# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

#which ever symbol is called must be ran on the input string, can include multiples
#each method symbol will point true or false or a parameter
#if changed to true, run that symbol as a method
#if given a number parameter, use that parameter
def transmogrify(string, o={})
  string_change = string
  opt = {
    :times => 0,
    :upcase => false,
    :reverse => false
  }.merge(o)

  opt.each do |key, val|
      string_change = string_change.send(key) if val == true
      string_change = string_change * val if val.is_a? Numeric
  end

  string_change
end
